<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//
require 'vendor/autoload.php';

$app = new \Slim\App;

$stad = "";
$manad = "";
$dag = "";

$app->group('/{stad}/{manad}', function() {
	$this->map(['GET'], '', function($req, $res) {
		$stad =  $req->getAttribute('stad');
		$manad =  $req->getAttribute('manad');

		$jsonfile =  file_get_contents('bonetider/' . $stad . '/' . $manad . '.json');
		$body = $res->getBody();
		$body->write($jsonfile);
		$lol = "lol";

		return $res->withHeader('Content-type', 'application/json');

	});

	$this->get('/{dag}', function($req, $res){
		$dag =  $req->getAttribute('dag');
		$stad =  $req->getAttribute('stad');
		$manad =  $req->getAttribute('manad');

		$jsonfile = file_get_contents('bonetider/' . $stad . '/' . $manad . '.json');
		$jsondec = json_decode($jsonfile, true);
		$jsonDag = json_encode($jsondec[$dag]);

		$res->getBody()->write($jsonDag);

		return $res
				
            ->withHeader('Access-Control-Allow-Origin', 'http://mysite')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
					->withHeader('Content-type', 'application/json');

	});
});


$app->run();

?>